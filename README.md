# WIDOCO Exploration

This document describes a simple guide for operating [WIDOCO](https://zenodo.org/record/583745).
WIDOCO is a software-based tool for generating set of documentations from a
given ontology file. The file can be provided from filesystem or fetched from
its URI on the Web.

## 1. Setup

To run WIDOCO, make sure all the following software has been installed:

1. [Java Runtime Engine (JRE) 8 or 9](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
2. [WIDOCO v1.4.1](https://github.com/dgarijo/Widoco/releases)

## 2. Running WIDOCO (GUI)

WIDOCO can be run as GUI-based application. To do so, double-click WIDOCO's JAR
file. A new window will be displayed on the screen.

![WIDOCO window](img/widoco_file_uri_1.png "Initial WIDOCO window showing step 1 of the wizard")

The application is run as a wizard application. It means that the application
give guided steps on how to process ontology file. The user need to follow
each steps as guided by the application.

There are 3 possible types of documentation template that can be created:

1. Create template documentation from ontology file
2. Create template documentation from ontology URI
3. Create empty skeleton

Each template type will be discussed in next subsections. The wizard steps for
creating documentation from ontology file and URL are similar.

### 2.1. From Ontology File/URI

Click the first option/radio button in the Step 1 window. WIDOCO will display
a pop-up window where the user can browse and choose ontology file to load. For
example, the following screenshot depicts loading ontology file from
[BBC](https://www.bbc.co.uk/ontologies/bbc):

![Choosing file](img/widoco_file_uri_2.png "Pop-up window for choosing file")

> Ontology files may come in various extensions. WIDOCO only recognises files
> with extension .ttl, .rdf, but not .xml. This can be circumvented by changing
> the *Files of type* option to **All files**.

Once finished choosing a file, the pop-up window will close and the path to
the chosen file is set in the main window. The project name and export location
can be set to other value if needed. Once done, click Next button. WIDOCO will
proceed to the next step.

In the second step, WIDOCO attempts to load metadata properties from the
ontology file. The process will take a while, depending on the size of the
ontology. Once it finishes, table elements in the main window will contain
properties and their values parsed from the ontology file.

![Properties & values](img/widoco_file_uri_3.png "Second step in WIDOCO wizard")

Values of each property can be edited as needed. For example, if certain
properties did lack values, user can set the values manually during this step.
The provided values from user in WIDOCO will not change the content in the
ontology file.

Once done setting properties, click Next button to continue to the next step.

In the third step, WIDOCO asks user to choose sections to include into the
documentation. User can choose to include sections based on parsed ontology
file or load it from different external file.

![Choosing sections](img/widoco_file_uri_4.png "Third step in WIDOCO wizard")

Once done selecting sections, click Generate! button to generate ontology
documentation at location given during first step

![Finish documentation](img/widoco_file_uri_5.png "Finished generating the documentation")

At the final step, it also possible to validate ontology using
[OOPS! (OntOlogy Pitfall Scanner!)](http://oops.linkeddata.es/) by clicking
the `Validate ontology with OOPS!` written in blue colour. The evaluation
result will be saved in the documentation folder.

![Validate ontology](img/widoco_file_uri_6.png "Validating ontology using OOPS!")

### 2.2. Empty Skeleton

Empty skeleton creates a new directory with an HTML file. The HTML file
has a basic ontology documentation structure that can be filled manually by
user.

## 3. Running WIDOCO (Console)

WIDOCO can be run via terminal/shell.

```bash
Usage: java -jar PATH_TO_WIDOCO_JAR [-ontFile file] or [-ontURI uri]
                                    [-outFolder folderName]
                                    [-confFile propertiesFile]
                                    [-getOntologyMetadata]
                                    [-oops]
                                    [-rewriteAll]
                                    [-crossRef]
                                    [-saveConfig configOutFile]
                                    [-lang lang1-lang2]
                                    [-includeImportedOntologies]
                                    [-htaccess]
                                    [-licensius]
                                    [-webVowl]
                                    [-ignoreIndividuals]
```

Example:

```bash
java -jar bin/WIDOCO.jar -ontFile ontologies/linkedearth.xml -outFolder out/linkedearth -getOntologyMetadata -oops -htaccess -webVowl -licensius
```

Refer to [this section](https://github.com/dgarijo/Widoco#how-to-use-widoco) at
official documentation for description of each option.

## Contributors

- [Daya Adianto](https://gitlab.com/addianto)

## License

[CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/)
